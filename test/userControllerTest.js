process.env.NODE_ENV = 'test';

let app = require('../src/app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let User = require('../src/api/models/UserModel');

chai.use(chaiHttp);

describe('User', () => {

    const user = {
        email: 'some@email.com',
        firstName: 'some-name',
        lastName: 'some-last-name',
        password: 'some-password'
    };

    before((done) => {
        User.remove({}, () => {
            done();
        });
    });

    describe('/POST user', () => {
        it('it should POST user', (done) => {
            chai.request(app)
                .post('/api/user')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email').eql(user.email);
                    res.body.should.have.property('lastName').eql(user.lastName);
                    res.body.should.have.property('firstName').eql(user.firstName);
                    res.body.should.have.property('password');
                    res.body.should.have.property('_id');
                    done();
                });
        });
    });

    describe('/LOGIN user', () => {
        it('it should LOGIN user with correct credentials', (done) => {
            chai.request(app)
                .post('/api/user/login')
                .send({
                    email: user.email,
                    password: user.password
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    done();
                });
        });
    });

    describe('/LOGIN user', () => {
        it('it should not LOGIN user with wrong credentials', (done) => {
            chai.request(app)
                .post('/api/user/login')
                .send({
                    email: 'wrong@email.com',
                    password: 'wrong-password'
                })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.not.have.property('token');
                    done();
                });
        });
    });

    describe('/PUT user', () => {
        it('it should UPDATE user', (done) => {
            chai.request(app)
                .post('/api/user/login')
                .send({
                    email: user.email,
                    password: user.password
                })
                .end((err, res) => {
                    chai.request(app)
                        .put('/api/user')
                        .set('Authorization', res.body.token)
                        .send({
                            email: 'new@email.com',
                            firstName: 'new-name',
                            lastName: 'new-last-name',
                            password: 'new-password',
                        }).end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('email').eql('new@email.com');
                        res.body.should.have.property('firstName').eql('new-name');
                        res.body.should.have.property('lastName').eql('new-last-name');
                        res.body.should.have.property('password');
                        done();
                    });

                });
        });
    });

    describe('/DELETE user', () => {
        it('it should DELETE user', (done) => {
            chai.request(app)
                .post('/api/user/login')
                .send({
                    email: 'new@email.com',
                    password: 'new-password'
                })
                .end((err, res) => {
                    chai.request(app)
                        .delete('/api/user')
                        .set('Authorization', res.body.token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('User deleted');
                            done();
                        });
                });
        });
    });

});