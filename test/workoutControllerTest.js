process.env.NODE_ENV = 'test';

let app = require('../src/app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let DateOnly = require('dateonly');
let should = chai.should();
let User = require('../src/api/models/UserModel');
let Workout = require('../src/api/models/WorkoutModel');

chai.use(chaiHttp);

describe('Workouts', () => {

    const email = 'tester@email.com';
    const password = 'tester123';
    let token;

    before((done) => {
        Workout.remove({}, () => {
        });
        User.remove({}, () => {
        });

        chai.request(app)
            .post('/api/user')
            .send({
                email: email,
                firstName: 'tester',
                lastName: 'tester',
                password: password
            })
            .end((err, res) => {
                res.should.have.status(200);
                chai.request(app)
                    .post('/api/user/login')
                    .send({
                        email: email,
                        password: password
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        token = res.body.token;
                        done();
                    });
            });
    });

    describe('/POST workout', () => {
        it('it should POST workout', (done) => {
            chai.request(app)
                .post('/api/workout')
                .set('Authorization', token)
                .send({
                    name: 'some-workout',
                    repetitions: 8,
                    series: 4
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').eql('some-workout');
                    res.body.should.have.property('repetitions').eql(8);
                    res.body.should.have.property('series').eql(4);
                    res.body.should.have.property('owner');
                    res.body.should.have.property('_id');
                    done();
                })
        });
    });

    describe('/GET workout', () => {
        it('it should GET all user workouts', (done) => {
            chai.request(app)
                .get('/api/workouts')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.have.property('name').eql('some-workout');
                    res.body[0].should.have.property('repetitions').eql(8);
                    res.body[0].should.have.property('series').eql(4);
                    res.body[0].should.have.property('owner');
                    res.body[0].should.have.property('_id');
                    done();
                })
        });
    });

    describe('/GET workout', () => {
        it('it should GET workouts based on regex on first word', (done) => {
            chai.request(app)
                .get('/api/workout/' + ' SoMe-WorkOut another-string')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.have.property('name').eql('some-workout');
                    res.body[0].should.have.property('repetitions').eql(8);
                    res.body[0].should.have.property('series').eql(4);
                    res.body[0].should.have.property('owner');
                    res.body[0].should.have.property('_id');
                    done();
                })
        });
    });

    describe('/GET workout by date', () => {
        it('it should GET workouts based on owner & date', (done) => {
            chai.request(app)
                .get('/api/workouts/' + new DateOnly(Date.now()))
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.have.property('name').eql('some-workout');
                    res.body[0].should.have.property('repetitions').eql(8);
                    res.body[0].should.have.property('series').eql(4);
                    res.body[0].should.have.property('owner');
                    res.body[0].should.have.property('_id');
                    done();
                })
        });
    });

    describe('/PUT workout', () => {
        it('it should UPDATE one user workouts', (done) => {
            chai.request(app)
                .post('/api/workout')
                .set('Authorization', token)
                .send({
                    name: 'some-workout',
                    repetitions: 8,
                    series: 4
                })
                .end((err, res) => {
                    chai.request(app)
                        .put('/api/workout/' + res.body._id)
                        .set('Authorization', token)
                        .send({
                            name: 'some-new-workout',
                            repetitions: 12,
                            series: 5
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('name').eql('some-new-workout');
                            res.body.should.have.property('repetitions').eql(12);
                            res.body.should.have.property('series').eql(5);
                            res.body.should.have.property('owner');
                            res.body.should.have.property('_id');
                            done();
                        })

                });
        });
    });

    describe('/DELETE workout', () => {
        it('it should DELETE workout from user workouts', (done) => {
            chai.request(app)
                .post('/api/workout')
                .set('Authorization', token)
                .send({
                    name: 'some-workout-to-delete',
                    repetitions: 8,
                    series: 4
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    chai.request(app)
                        .delete('/api/workout/' + res.body._id)
                        .set('Authorization', token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property('owner').eql(null);
                            done();
                        });
                })
        });
    });

});