process.env.NODE_ENV = 'test';

let app = require('../src/app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let DateOnly = require('dateonly');
let should = chai.should();
let User = require('../src/api/models/UserModel');
let Product = require('../src/api/models/ProductModel');

chai.use(chaiHttp);

describe('Products', () => {

    const email = 'tester@email.com';
    const password = 'tester123';
    let token;

    before((done) => {
        Product.remove({}, () => {
        });
        User.remove({}, () => {
        });

        chai.request(app)
            .post('/api/user')
            .send({
                email: email,
                firstName: 'tester',
                lastName: 'tester',
                password: password
            })
            .end((err, res) => {
                res.should.have.status(200);
                chai.request(app)
                    .post('/api/user/login')
                    .send({
                        email: email,
                        password: password
                    })
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        token = res.body.token;
                        done();
                    });
            });
    });

    describe('/POST product', () => {
        it('it should POST product', (done) => {
            chai.request(app)
                .post('/api/product')
                .set('Authorization', token)
                .send({
                    name: 'some-product',
                    calories: 1234,
                    category: 'some-category'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('calories').eql(1234);
                    res.body.should.have.property('name').eql('some-product');
                    res.body.should.have.property('category').eql('some-category');
                    res.body.should.have.property('owner');
                    res.body.should.have.property('_id');
                    done();
                })
        });
    });

    describe('/GET product', () => {
        it('it should GET all user products', (done) => {
            chai.request(app)
                .get('/api/products')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.have.property('calories').eql(1234);
                    res.body[0].should.have.property('name').eql('some-product');
                    res.body[0].should.have.property('category').eql('some-category');
                    res.body[0].should.have.property('owner');
                    res.body[0].should.have.property('_id');
                    done();
                })
        });
    });

    describe('/GET product', () => {
        it('it should GET products based on regex on first word', (done) => {
            chai.request(app)
                .get('/api/product/' + ' SoMe-ProducT another-string')
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.have.property('calories').eql(1234);
                    res.body[0].should.have.property('name').eql('some-product');
                    res.body[0].should.have.property('category').eql('some-category');
                    res.body[0].should.have.property('owner');
                    res.body[0].should.have.property('_id');
                    done();
                })
        });
    });

    describe('/GET product by date', () => {
        it('it should GET products based on owner & date', (done) => {
            chai.request(app)
                .get('/api/products/' + new DateOnly(Date.now()))
                .set('Authorization', token)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body[0].should.have.property('calories').eql(1234);
                    res.body[0].should.have.property('name').eql('some-product');
                    res.body[0].should.have.property('category').eql('some-category');
                    res.body[0].should.have.property('owner');
                    res.body[0].should.have.property('_id');
                    done();
                })
        });
    });

    describe('/PUT product', () => {
        it('it should UPDATE one user products', (done) => {
            chai.request(app)
                .post('/api/product')
                .set('Authorization', token)
                .send({
                    name: 'some-product',
                    calories: 1234,
                    category: 'some-category'
                })
                .end((err, res) => {
                    chai.request(app)
                        .put('/api/product/' + res.body._id)
                        .set('Authorization', token)
                        .send({
                            name: 'some-new-product',
                            calories: 4321,
                            category: 'some-new-category'
                        })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('calories').eql(4321);
                            res.body.should.have.property('name').eql('some-new-product');
                            res.body.should.have.property('category').eql('some-new-category');
                            res.body.should.have.property('owner');
                            res.body.should.have.property('_id');
                            done();
                        })

                });
        });
    });

    describe('/DELETE product', () => {
        it('it should DELETE product from user products', (done) => {
            chai.request(app)
                .post('/api/product')
                .set('Authorization', token)
                .send({
                    name: 'some-product-to-delete',
                    calories: 1234,
                    category: 'some-category'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    chai.request(app)
                        .delete('/api/product/' + res.body._id)
                        .set('Authorization', token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property('owner').eql(null);
                            done();
                        });
                })
        });
    });

});