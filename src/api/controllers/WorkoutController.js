let mongoose = require('mongoose'),
    DateOnly = require('dateonly'),
    Workout = require('../models/WorkoutModel');

exports.create = (req, res) => {
    const owner = req.user._id;
    const name = req.body.name;
    const repetitions = req.body.repetitions;
    const series = req.body.series;

    if(!name){
        return res.status(400).json({message: 'Required name parameter is missing.'});
    }

    let newWorkout = Workout({
        name: name,
        repetitions: repetitions,
        series: series,
        owner: owner
    });

    newWorkout.save((err, workout) => {
        if (err) return res.status(400).send(err);
        return res.json(workout);
    });
};

exports.getByOwner = (req, res) => {
    const owner = req.user._id;

    Workout.find({owner: owner}, (err, workouts) => {
        if (err) return res.status(400).send(err);
        return res.json(workouts);
    })

};

exports.update = (req, res) => {
    const owner = req.user_id;
    const id = req.params._id;

    Workout.find({_id: id, owner: owner}, (err, workout) => {
        if (err) return res.status(400).send(err);
        if (workout) {
            Workout.findByIdAndUpdate(id, req.body, {upsert: true, new: true}, (err, workout) => {
                if (err) return res.status(400).send(err);
                return res.json(workout);
            });
        } else {
            return res.status(404).json({message: 'Not found'});
        }
    });

};

exports.getByNameRegex = (req, res) => {
    const reqName = req.params.name;
    let name = reqName.split(' ')[0];

    Workout.find({name: {$regex: name, $options: 'igmxs'}}).limit(10).exec((err, workout) => {
        if (err) return res.status(400).send(err);
        return res.json(workout);
    });
};

exports.deleteFromUserWorkouts = (req, res) => {
    const id = req.params._id;
    const owner = req.user._id;

    Workout.find({_id: id, owner: owner}, (err, workout) => {
        if (workout) {
            Workout.findByIdAndUpdate(id, {owner: null}, {upsert: true, new: true}, (err, workout) => {
                if (err) return res.status(400).send(err);
                return res.json(workout);
            });
        } else {
            return res.status(404).json({message: 'Not found'});
        }
    });
};

exports.getByOwnerAndDate = (req, res) => {
    const owner = req.user._id;
    const date = Number(req.params.date);

    Workout.find({owner: owner, created: date}, (err, workouts) => {
        if (workouts) {
            if (err) return res.status(400).send(err);
            return res.json(workouts);
        } else {
            return res.status(404).json({message: 'Not found'});
        }
    });
};

