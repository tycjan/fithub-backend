let crypt = require('bcrypt'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    User = require('../models/UserModel');

exports.create = (req, res) => {
    if (req.body.password) {
        let newUser = new User(req.body);
        newUser.password = crypt.hashSync(req.body.password, 10);
        newUser.save((err, user) => {
            if (err) return res.status(400).send(err);
            return res.json(user);
        });
    } else {
        return res.status(400).send({message: "Required password parameter"});
    }
};

exports.login = (req, res) => {
    User.findOne({email: req.body.email}, (err, user) => {
        if (user && req.body.password && user.comparePassword(req.body.password)) {
            return res.json({token: jwt.sign({email: user.email, name: user.name, _id: user._id}, 'fithub_token')});
        }
        return res.status(401).json({message: 'Authentication failed.'});
    });
};

exports.get = (req, res) => {
    const id = req.user._id;

    User.findById(id, (err, user) => {
        if (err) return res.status(400).send(err);
        return res.json(user)
    })
};

exports.update = (req, res) => {
    const id = req.user._id;

    User.findById(id, (err, user) => {
        if (err) return res.status(400).send(err);
        else if (user) {
            if (req.body.password) {
                req.body.password = crypt.hashSync(req.body.password, 10);
            }
            User.findByIdAndUpdate(id, req.body, {upsert: true, new: true}, (err, user) => {
                if (err) return res.status(400).send(err);
                return res.json(user);
            });
        }
    });
};

exports.delete = (req, res) => {
    const id = req.user._id;

    User.findByIdAndRemove(id, (err) => {
        if (err) return res.status(400).send(err);
        return res.status(200).json({message: 'User deleted'});
    });
};

exports.access = (req, res, next) => {
    if (req.user) {
        next();
    } else {
        return res.status(401).json({message: 'Unauthorized user!'});
    }
};