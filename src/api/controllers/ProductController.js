let mongoose = require('mongoose'),
    DateOnly = require('dateonly'),
    Product = require('../models/ProductModel');

exports.create = (req, res) => {
    const owner = req.user._id;
    const name = req.body.name;
    const calories = req.body.calories;
    let category = 'none';

    if(!name || !calories){
        return res.status(400).json({message: 'Required parameters missing.'});
    }

    if (req.body.category) category = req.body.category;

    let newProduct = Product({
        name: name,
        calories: calories,
        owner: owner,
        category: category
    });

    newProduct.save((err, product) => {
        if (err) return res.status(400).send(err);
        return res.json(product);
    });
};

exports.getByOwner = (req, res) => {
    const owner = req.user._id;

    Product.find({owner: owner}, (err, products) => {
        if (err) return res.status(400).send(err);
        return res.json(products);
    })

};

exports.update = (req, res) => {
    const owner = req.user_id;
    const id = req.params._id;

    Product.find({_id: id, owner: owner}, (err, product) => {
        if (err) return res.status(400).send(err);
        if (product) {
            Product.findByIdAndUpdate(id, req.body, {upsert: true, new: true}, (err, product) => {
                if (err) return res.status(400).send(err);
                return res.json(product);
            });
        } else {
            return res.status(404).json({message: 'Not found'});
        }
    });

};

exports.getByNameRegex = (req, res) => {
    const reqName = req.params.name;
    let name = reqName.split(' ')[0];

    Product.find({name: {$regex: name, $options: 'igmxs'}}).limit(10).exec((err, product) => {
        if (err) return res.status(400).send(err);
        return res.json(product);
    });
};

exports.deleteFromUserProducts = (req, res) => {
    const id = req.params._id;
    const owner = req.user._id;

    Product.find({_id: id, owner: owner}, (err, product) => {
        if (product) {
            Product.findByIdAndUpdate(id, {owner: null}, {upsert: true, new: true}, (err, product) => {
                if (err) return res.status(400).send(err);
                return res.json(product);
            });
        } else {
            return res.status(404).json({message: 'Not found'});
        }
    });
};

exports.getByOwnerAndDate = (req, res) => {
    const owner = req.user._id;
    const date = Number(req.params.date);

    Product.find({owner: owner, created: date}, (err, products) => {
        if (products) {
            if (err) return res.status(400).send(err);
            return res.json(products);
        } else {
            return res.status(404).json({message: 'Not found'});
        }
    });
};

