let mongoose = require('mongoose'),
    crypt = require('bcrypt'),
    Schema = mongoose.Schema;

let UserSchema = new Schema({
    firstName: {
        type: String,
        trim: true,
        default: '',
        required: true
    },

    lastName: {
        type: String,
        trim: true,
        default: '',
        required: true
    },

    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        default: '',
        required: true
    },

    password: {
        type: String,
        default: '',
        required: true
    },

    created: {
        type: Date,
        default: Date.now
    }
});

UserSchema.methods.comparePassword = function(password) {
    return crypt.compareSync(password, this.password)
};

mongoose.model('User', UserSchema);

module.exports = mongoose.model('Users', UserSchema);