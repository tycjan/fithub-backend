let mongoose = require('mongoose'),
    DateOnly = require('mongoose-dateonly')(mongoose);
    Schema = mongoose.Schema;

let ProductSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true,
    },

    calories: {
        type: Number,
        default: 0,
        required: true,
    },

    category: {
        type: String,
        default: 'none'
    },

    created: {
        type: DateOnly,
        default: new DateOnly(Date.now()),
    },

    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }

});

mongoose.model('Product', ProductSchema);

module.exports = mongoose.model('Product', ProductSchema);
