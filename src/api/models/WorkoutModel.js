let mongoose = require('mongoose'),
    DateOnly = require('mongoose-dateonly')(mongoose);
    Schema = mongoose.Schema;

let WorkoutSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: true,
    },

    repetitions: {
        type: Number,
        default: 0,
        required: true,
    },

    series: {
        type: Number,
        default: 0,
        required: true,
    },

    created: {
        type: DateOnly,
        default: new DateOnly(Date.now()),
    },

    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }

});

mongoose.model('Workout', WorkoutSchema);

module.exports = mongoose.model('Workout', WorkoutSchema);
