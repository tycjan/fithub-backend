module.exports = (app) => {

    let user = require('../controllers/UserController');
    let product = require('../controllers/ProductController');
    let workout = require('../controllers/WorkoutController');

    app.route('/api/user')
        .post(user.create)
        .get(user.access, user.get)
        .put(user.access, user.update)
        .delete(user.access, user.delete);

    app.route('/api/user/login')
        .post(user.login);

    app.route('/api/product')
        .post(user.access, product.create);

    app.route('/api/products')
        .get(user.access, product.getByOwner);

    app.route('/api/products/:date')
        .get(user.access, product.getByOwnerAndDate);

    app.route('/api/product/:_id')
        .delete(user.access, product.deleteFromUserProducts)
        .put(user.access, product.update);

    app.route('/api/product/:name')
        .get(user.access, product.getByNameRegex);
    
    app.route('/api/workout')
        .post(user.access, workout.create);

    app.route('/api/workouts')
        .get(user.access, workout.getByOwner);

    app.route('/api/workouts/:date')
        .get(user.access, workout.getByOwnerAndDate);

    app.route('/api/workout/:_id')
        .delete(user.access, workout.deleteFromUserWorkouts)
        .put(user.access, workout.update);

    app.route('/api/workout/:name')
        .get(user.access, workout.getByNameRegex);
};