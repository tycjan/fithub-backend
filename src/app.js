let express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    db = 'mongodb://jackwhite:metrobot777@ds145183.mlab.com:45183/fithub',
    host = 'https://fithubserver.herokuapp.com:',
    User = require('./api/models/UserModel'),
    Product = require('./api/models/ProductModel'),
    Workout = require('./api/models/WorkoutModel'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    jsonwebtoken = require("jsonwebtoken"),
    cors = require('cors'),
    routes = require('./api/routes/Routes');

mongoose.Promise = global.Promise;

if (process.env.NODE_ENV === 'test') {
    port = 3002;
    host = 'http://localhost:';
    db = process.env.MONGO_URI || 'mongodb://localhost/fithubTest';
}

if (process.env.NODE_ENV === 'development') {
    port = 3000;
    host = 'http://localhost:';
    db = 'mongodb://localhost/fithub';
}

mongoose.connect(db, {useMongoClient: true}).catch((err) => {
    console.log(err);
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(bodyParser.json());
app.use(cors());

app.use((req, res, next) => {
    if (req.headers.authorization) {
        jsonwebtoken.verify(req.headers.authorization, 'fithub_token', (err, decode) => {
            if (err) req.user = undefined;
            req.user = decode;
            next();
        });
    } else {
        req.user = undefined;
        next();
    }
});

routes(app);

app.use((req, res) => {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('FitHub started at ' + host + port+'/');

module.exports = app;

